rs-immut_string
=====

immutable string

```rust
extern crate immut_string;

use immut_string::ImmutString;

fn main() {
  let a: ImmutString = "Hello".into();
  let b = ImmutString::from(", world!");
  let c = a + b;

  assert_eq!(c, "Hello, world!");
  assert_eq!(c.len(), 13);
}
```
